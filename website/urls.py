from django.urls import path
from . import views

urlpatterns = [
    path('', views.get_client_info, name="index"),
    path('add-tracking', views.add_tracking, name="add-tracking"),
    path('last-tracking-by-device', views.last_tracking_by_device, name="last_tracking_by_device")
]