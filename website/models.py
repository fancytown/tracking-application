from django.db import models
from django.contrib.auth.models import User


class WebsiteModel(models.Model):
    latitude = models.CharField(max_length=250, null=False)
    longitude = models.CharField(max_length=250, null=False)

    class Meta:
        managed = False
        db_table = 'website_user'

    def __str__(self):
        return "Latitude: " + str(self.latitude) + "\t" + "Longitude: " + str(self.longitude) + "\n"
