import time
from urllib import response


class StatsMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_request(self, request):
        # "Start time at request coming in"
        request.start_time = time.time()

    def process_response(self, request, response):
        # "End of request, take time"
        total = time.time() - request.start_time
        response["X-total-time"] = int(total * 1000)
        return response
